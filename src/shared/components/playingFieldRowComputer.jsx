import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { registerShoot } from '../services/game';

const PlayingFieldRowComputer = (props) => {
    return (
        <Fragment>
            <div className="col s2">{props.index}</div>
            <div className="playingField col s1" onClick={(e) => shoot('A', e)}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'B')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'C')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'D')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'E')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'F')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'G')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'H')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'I')}></div>
            <div className="playingField col s1" onClick={() => registerShoot(props.gameId, props.index, 'J')}></div>
        </Fragment>
    );

    function shoot(column, e) {
        let hit = false;

        registerShoot(props.gameId, props.index, column).then(r => {
            r.text().then(result => {
                result = JSON.parse(result);

                hit = result.hit;
            });
        });

        setTimeout(function (hit, e) {

            if (hit) {
                e.target.style.backgroundColor = 'red';
            } else {
                e.target.style.backgroundColor = 'blue';
            }
        }, 5000);
    }
}

PlayingFieldRowComputer.propTypes = {
    gameId: PropTypes.number,
    index: PropTypes.number,
}

export default PlayingFieldRowComputer;